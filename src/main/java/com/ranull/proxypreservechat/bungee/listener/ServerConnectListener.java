package com.ranull.proxypreservechat.bungee.listener;

import com.ranull.proxypreservechat.bungee.ProxyPreserveChat;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;

public class ServerConnectListener implements Listener {
    private final ProxyPreserveChat plugin;

    public ServerConnectListener(ProxyPreserveChat plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onServerConnect(ServerConnectEvent event) {
        if (event.getReason() == ServerConnectEvent.Reason.JOIN_PROXY) {
            plugin.getMessageManager().sendMessages(event.getPlayer(), event.getTarget());
        }
    }
}
