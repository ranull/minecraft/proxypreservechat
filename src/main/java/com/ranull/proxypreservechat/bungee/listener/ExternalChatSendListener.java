package com.ranull.proxypreservechat.bungee.listener;

import com.ranull.proxychatbridge.bungee.event.ExternalChatSendEvent;
import com.ranull.proxypreservechat.bungee.ProxyPreserveChat;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;

public class ExternalChatSendListener implements Listener {
    private final ProxyPreserveChat plugin;

    public ExternalChatSendListener(ProxyPreserveChat plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onExternalChatSendEvent(ExternalChatSendEvent event) {
        if (!event.isCancelled() && (event.getSource() != null && plugin.getConfig().getStringList("preserve.sending")
                .stream().anyMatch(string -> event.getSource().matches(string)))) {
            plugin.getMessageManager().addMessage(event.getUUID(), event.getName(), event.getFormat(),
                    event.getMessage(), event.getGroup(), event.getSource());
        }
    }
}
