package com.ranull.proxypreservechat.bungee.listener;

import com.ranull.proxychatbridge.bungee.event.ExternalChatReceiveEvent;
import com.ranull.proxypreservechat.bungee.ProxyPreserveChat;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;

public class ExternalChatReceiveListener implements Listener {
    private final ProxyPreserveChat plugin;

    public ExternalChatReceiveListener(ProxyPreserveChat plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onExternalChatReceive(ExternalChatReceiveEvent event) {
        if (!event.isCancelled() && (event.getSource() != null && plugin.getConfig().getStringList("preserve.receiving")
                .stream().anyMatch(string -> event.getSource().matches(string)))) {
            plugin.getMessageManager().addMessage(event.getUUID(), event.getName(), event.getFormat(),
                    event.getMessage(), event.getGroup(), event.getSource());
        }
    }
}
