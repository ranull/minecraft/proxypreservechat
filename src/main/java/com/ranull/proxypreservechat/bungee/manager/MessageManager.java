package com.ranull.proxypreservechat.bungee.manager;

import com.ranull.proxypreservechat.bungee.ProxyPreserveChat;
import com.ranull.proxypreservechat.bungee.cache.CachedMessage;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class MessageManager {
    private final ProxyPreserveChat plugin;
    private final HashMap<String, List<CachedMessage>> hashMap;

    public MessageManager(ProxyPreserveChat plugin) {
        this.plugin = plugin;
        this.hashMap = new HashMap<>();
    }

    public void addMessage(UUID uuid, String name, String format, String message, String group, String source) {
        CachedMessage cachedMessage = new CachedMessage(uuid, name, format, message, group, source);

        if (hashMap.containsKey(group)) {
            hashMap.get(group).add(cachedMessage);
        } else {
            List<CachedMessage> cachedMessageList = new ArrayList<>();

            cachedMessageList.add(cachedMessage);
            hashMap.put(group, cachedMessageList);
        }

        if (hashMap.get(group).size() == 51) {
            hashMap.get(group).remove(0);
        }
    }

    @SuppressWarnings("deprecation")
    public void sendMessages(ProxiedPlayer proxiedPlayer, ServerInfo serverInfo) {
        if (isValidServer(serverInfo.getName())) {
            String group = getGroup(serverInfo.getName());

            if (hashMap.containsKey(group)) {
                for (CachedMessage cachedMessage : hashMap.get(group)) {
                    proxiedPlayer.sendMessage(cachedMessage.getFormat()
                            .replace("%1$s", cachedMessage.getName())
                            .replace("%2$s", cachedMessage.getMessage()));
                }
            }
        }
    }

    public boolean isValidServer(String serverName) {
        String mode = plugin.getConfig().getString("mode");
        List<String> stringList = plugin.getConfig().getStringList("servers");

        return (mode.equalsIgnoreCase("whitelist") && stringList.contains(serverName))
                || (mode.equalsIgnoreCase("blacklist") && !stringList.contains(serverName));
    }

    public String getGroup(String serverName) {
        return plugin.getProxyChatBridgeConfig().getString("servers." + serverName + ".group",
                plugin.getProxyChatBridgeConfig().getString("group", "global"));
    }
}
