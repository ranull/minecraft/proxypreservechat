package com.ranull.proxypreservechat.bungee.manager;

import com.google.common.io.ByteStreams;
import com.ranull.proxypreservechat.bungee.ProxyPreserveChat;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

public class ConfigManager {
    private final ProxyPreserveChat plugin;
    private Configuration configFile;
    private Configuration proxyChatBridgeConfigFile;

    public ConfigManager(ProxyPreserveChat plugin) {
        this.plugin = plugin;

        saveConfig();
        loadConfig();
        loadProxyChatBridgeConfig();
    }

    public Configuration getConfig() {
        return configFile;
    }

    public Configuration getProxyChatBridgeConfig() {
        return proxyChatBridgeConfigFile;
    }

    public void saveConfig() {
        if (plugin.getDataFolder().exists() || plugin.getDataFolder().mkdirs()) {
            File configFile = new File(plugin.getDataFolder(), "config.yml");

            if (!configFile.exists()) {
                try {
                    if (configFile.createNewFile()) {
                        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("config.yml");

                        if (inputStream != null) {
                            ByteStreams.copy(inputStream, Files.newOutputStream(configFile.toPath()));
                        }
                    }
                } catch (IOException exception) {
                    exception.printStackTrace();
                }
            }
        }
    }

    public void loadConfig() {
        try {
            configFile = ConfigurationProvider.getProvider(YamlConfiguration.class)
                    .load(new File(plugin.getDataFolder(), "config.yml"));
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public void loadProxyChatBridgeConfig() {
        try {
            proxyChatBridgeConfigFile = ConfigurationProvider.getProvider(YamlConfiguration.class)
                    .load(new File(getProxyChatBridgeDataFolder(), "config.yml"));
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    private File getProxyChatBridgeDataFolder() {
        return new File(plugin.getDataFolder().getParentFile(), "ProxyChatBridge");
    }
}
