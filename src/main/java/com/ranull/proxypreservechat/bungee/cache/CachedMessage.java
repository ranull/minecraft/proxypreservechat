package com.ranull.proxypreservechat.bungee.cache;

import java.util.UUID;

public class CachedMessage {
    private final UUID uuid;
    private final String name;
    private final String format;
    private final String message;
    private final String group;
    private final String source;

    public CachedMessage(UUID uuid, String name, String format, String message, String group, String source) {
        this.uuid = uuid;
        this.name = name;
        this.format = format;
        this.message = message;
        this.group = group;
        this.source = source;
    }

    public UUID getUUID() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    public String getFormat() {
        return format;
    }

    public String getMessage() {
        return message;
    }

    public String getGroup() {
        return group;
    }

    public String getSource() {
        return source;
    }
}
