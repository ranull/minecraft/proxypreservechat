package com.ranull.proxypreservechat.bungee;

import com.ranull.proxypreservechat.bungee.command.ProxyPreserveChatCommand;
import com.ranull.proxypreservechat.bungee.listener.ExternalChatReceiveListener;
import com.ranull.proxypreservechat.bungee.listener.ExternalChatSendListener;
import com.ranull.proxypreservechat.bungee.listener.ServerConnectListener;
import com.ranull.proxypreservechat.bungee.manager.ConfigManager;
import com.ranull.proxypreservechat.bungee.manager.MessageManager;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import org.bstats.bungeecord.MetricsLite;

public class ProxyPreserveChat extends Plugin {
    private ConfigManager configManager;
    private MessageManager messageManager;

    @Override
    public void onEnable() {
        configManager = new ConfigManager(this);
        messageManager = new MessageManager(this);

        registerMetrics();
        registerListeners();
        registerCommands();
    }

    private void registerMetrics() {
        new MetricsLite(this, 17252);
    }

    private void registerListeners() {
        getProxy().getPluginManager().registerListener(this, new ExternalChatReceiveListener(this));
        getProxy().getPluginManager().registerListener(this, new ExternalChatSendListener(this));
        getProxy().getPluginManager().registerListener(this, new ServerConnectListener(this));
    }

    private void registerCommands() {
        getProxy().getPluginManager().registerCommand(this, new ProxyPreserveChatCommand(this));
    }

    public MessageManager getMessageManager() {
        return messageManager;
    }

    public Configuration getConfig() {
        return configManager.getConfig();
    }

    public Configuration getProxyChatBridgeConfig() {
        return configManager.getProxyChatBridgeConfig();
    }

    public void reloadConfig() {
        configManager.loadConfig();
        configManager.loadProxyChatBridgeConfig();
    }
}
