package com.ranull.proxypreservechat.bungee.command;

import com.ranull.proxypreservechat.bungee.ProxyPreserveChat;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

public class ProxyPreserveChatCommand extends Command {
    private final ProxyPreserveChat plugin;

    public ProxyPreserveChatCommand(ProxyPreserveChat plugin) {
        super("proxypreservechat", null, "bungeepreservechat", "velocitypreservechat", "ppc", "bpc", "vpc");
        this.plugin = plugin;
    }

    @SuppressWarnings("deprecation")
    public void execute(CommandSender commandSender, String[] args) {
        if (args.length == 0) {
            commandSender.sendMessage(ChatColor.LIGHT_PURPLE + "✉" + ChatColor.DARK_GRAY + " » " + ChatColor.LIGHT_PURPLE
                    + "ProxyPreserveChat " + ChatColor.DARK_GRAY + "v" + plugin.getDescription().getVersion());
            commandSender.sendMessage(
                    ChatColor.GRAY + "/ppc " + ChatColor.DARK_GRAY + "-" + ChatColor.RESET + " Plugin info");

            if (commandSender.hasPermission("proxypreservechat.reload")) {
                commandSender.sendMessage(ChatColor.GRAY + "/ppc reload " + ChatColor.DARK_GRAY + "-" + ChatColor.RESET
                        + " Reload plugin");
            }

            commandSender.sendMessage(ChatColor.DARK_GRAY + "Author: " + ChatColor.GRAY + "Ranull");
        } else if (args[0].equals("reload")) {
            if (commandSender.hasPermission("proxypreservechat.reload")) {
                plugin.reloadConfig();
                commandSender.sendMessage(ChatColor.LIGHT_PURPLE + "✉" + ChatColor.DARK_GRAY + " » "
                        + ChatColor.RESET + "Reloaded config file.");
            } else {
                commandSender.sendMessage(ChatColor.LIGHT_PURPLE + "✉" + ChatColor.DARK_GRAY + " » "
                        + ChatColor.RESET + "No permission.");
            }
        }
    }
}
